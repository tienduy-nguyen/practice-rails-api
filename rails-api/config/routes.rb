Rails.application.routes.draw do
  # devise_for :users
  namespace :api, defaults: { format: :json } do
    resources :articles
  end

  devise_for :users,
    defaults: { format: :json },
    path: "",
    path_names: {
      sign_in: "api/auth/login",
      sign_out: "api/auth/logout",
      registration: "api/auth/register",
    },
    controllers: {
      sessions: "sessions",
      registrations: "registrations",
    }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
