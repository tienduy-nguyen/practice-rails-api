# Rails API

## Getting started

- Create project
  ```rb
  $ rails new my_api --api
  ```

  This will do three main things for you:

  - Configure your application to start with a more limited set of middleware than normal. Specifically, it will not include any middleware primarily useful for browser applications (like cookies support) by default.
  - Make `ApplicationController` inherit from `ActionController::API` instead of `ActionController::Base`. As with middleware, this will leave out any Action Controller modules that provide functionalities primarily used by browser applications.
  - Configure the generators to skip generating views, helpers, and assets when you generate a new resource.

## CRUD
- Scaffold quickly
  ```bash
  $ rails g scaffold Article title:string content:text
  ```
- Change default port in `config/puma.rb`
  ```rb
  #
  port ENV.fetch("PORT") { 4120 }
  ```

## Reference

- [Official rails guide](https://guides.rubyonrails.org/api_app.html)
- [Important notes for sass](https://github.com/rails/sass-rails#important-note)
- [Another guide devise-jwt](https://jameschambers.co.uk/rails-api)